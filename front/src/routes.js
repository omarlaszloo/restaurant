// We only need to import the modules necessary for initial render
import CoreLayout from './layouts/CoreLayout'
import App from './containers/home/index'
import Home from './containers/home'
import Admin from './containers/admin'
import Users from './containers/users'
import Comandas from './containers/comandas'


/*  Note: Instead of using JSX, we recommend using react-router
    PlainRoute objects to build route definitions.   */

export const createRoutes = store => ({
  path        : '/',
  component   : CoreLayout,
  indexRoute  : Home,
  childRoutes : [
    CoreLayout(store),
    Home(store),
    Admin(store),
    Users(store),
    Comandas(store)
  ]
})

export default createRoutes
