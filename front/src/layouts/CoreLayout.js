import React from 'react'
import { Link } from 'react-router'

const menuItems = [
  { label : 'Home', link: '/home' },
  { label : 'Admin', link: '/admin' },
  { label : 'Users', link: '/users' },
  { label : 'Comandas', link: '/comandas' },
]

export const CoreLayout = ({ children }) => (
  <div className=''>
    <div style={{ display: 'flex', justifyContent: 'center', margin: '2%' }}>
      {menuItems.map((item) => (
          <Link className='link' to={item.link} activeStyle={{ color: '#0d47a1' }} style={{ margin: '0 2% 0 0'}}>
            {item.label}
          </Link>
        )
      )}
    </div>
   <div>
      {children}
    </div>
  </div>
)

export default CoreLayout
