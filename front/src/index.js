import React from 'react'
import { render } from 'react-dom'
import App from './containers/app'
import createStore from './store/store'
// import store, { history } from './store/store'
// import routes from './routes'

import './styles/index.css'
const initialState = window.___INITIAL_STATE__
const store = createStore(initialState)
const target = document.querySelector('#root')
const routes = require('./routes').default(store)
render(
  <div>
    <App store={store} routes={routes} />
  </div>,
  target
)
