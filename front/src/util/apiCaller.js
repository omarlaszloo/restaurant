import fetch from 'isomorphic-fetch'

export const API_URL = process.env.NODE_ENV === 'production'
  ? 'http://localhost:4000/api' : 'http://localhost:4000/api'

export const callApi = (endpoint, method = 'get', body) => {
  console.log('in jax api calleer')
  return fetch(`${API_URL}/${endpoint}`)
  .then(response => response.json().then(json => ({ json, response })))
  .then(({ json, response }) => {
    if (!response.ok) {
      return Promise.reject(json)
    }
    return json
  })
  .then(
    response => response,
    error => error
  )
}


export const callApiByForm = (endpoint, method = 'get', body) => {
  console.log('body', body)
  let formData = new FormData()

  for (let name in body) {
    formData.append(name, body[name])
  }
  

  console.log('formDAta', formData)
  return fetch(`${API_URL}/${endpoint}`, {
    method,
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body)
  })
  .then(response => response.json().then(json => ({ json, response })))
  .then(({ json, response }) => {
    if (!response.ok) {
      return Promise.reject(json)
    }

    return json
  })
  .then(
    response => response,
    error => error
  )
}
