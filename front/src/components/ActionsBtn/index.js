import React, { PropTypes } from 'react'
import './index.css'

const ActionsBtn = props => (
  <div className='btn-actions'>
    <button
      type='submit'
      className='btn-delete'>
      {props.titleCancel}
    </button>
    <button
      type='submit'
      onClick={() => props.handleClick}
      className='btn-primary'>
      {props.titleAcept}
    </button>
  </div>
)
ActionsBtn.propTypes = {
  handleClick: PropTypes.func
}

export default ActionsBtn