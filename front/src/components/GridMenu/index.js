import React from 'react'
import NumberFormat from 'react-number-format'

const GridMenu = props => {
  const {
    handleAdd,
    gridClass,
    menu,
    cardFood,
    cardDescription
  } = props
  return(
    <div className={gridClass} style={{ padding: '2%' }}>
      {menu.map((item) => {
        return (
          <article
            onClick={() => handleAdd(item)}
            className={cardFood}>
            <div className={cardDescription}>
              <p style={{ textTransform: 'uppercase' }}>{item.name}</p>
                <br/>
                <p><NumberFormat
                value={item.price}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'$'} />
              </p>
            </div>
          </article>
        )
      })}
    </div>
  )
}

export default GridMenu