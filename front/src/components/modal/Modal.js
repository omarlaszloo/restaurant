import React from 'react'
import './Modal.css'

const Modal = () => {
  console.log('Modal')
  return (
    <div class='modal-wrapper' id="popup">
    <div class='popup-contenedor'>
      <h2>Titulo de la Modal</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor deleniti...</p>
      <a class='popup-cerrar' href="#">X</a>
    </div>
  </div>
  )
}

export default Modal