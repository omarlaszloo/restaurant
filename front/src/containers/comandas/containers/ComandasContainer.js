import { connect } from 'react-redux'
import {
  fetchComandas
} from '../ComandasActions'

import ComandasView from '../components/ComandasView'

import { getComandas } from '../ComandasReducer'

const mapDispatchToProps = {
  fetchComandas
}

const mapStateToProps = (state, dispatch) => {
  return {
    comandas: getComandas(state),
    id: state  
  }
}

const temp = connect(mapStateToProps, mapDispatchToProps)(ComandasView)

class Container extends temp {
  componentWillMount () {
    this.store.dispatch(fetchComandas())
  }

}

export default Container
