import { injectReducer } from '../../store/reducers'
import ComandasContainer from './containers/ComandasContainer'
import reducer from './ComandasReducer'


export default(store) => ({
  path: 'comandas',
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      /*  Add the reducer to the store on key 'counter'  */
      injectReducer(store, {
        key: 'comandas',
        reducer
      })
    
      /*  Return getComponent   */
      cb(null, ComandasContainer)

      /* Webpack named bundle   */
    }, 'AdminView')
  }
})
