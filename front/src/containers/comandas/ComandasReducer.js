import _ from 'lodash'
import {
  ADD_COMANDAS
} from './ComandasActions'


// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [ADD_COMANDAS]: (state, action) => ({
    data: action.data,
    id: 1,
  }),
}

// Initial State
const initialState = {
  data: [],
  id: 0
}

export default function ComandasReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

// get all menu
export const getComandas = state => state.comandas.data

