import { callApi, callApiByForm } from '../../util/apiCaller'

export const ADD_COMANDAS = 'ADD_COMANDAS'

export const addComandas = data => ({ type: ADD_COMANDAS, data })

export const fetchComandas = () => {
  return dispatch => (callApi('user').then(res => {
    console.log('COMANDAS    >>>', res.users)
    dispatch(addComandas(res.users))
  }))
}
