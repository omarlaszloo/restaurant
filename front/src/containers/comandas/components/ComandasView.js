import React, { Component } from 'react'
import './ComandasView.css'

class ComandasView extends Component {
  constructor(props){
    super(props)
    this.state = {
      lists: [],
      preview: []
    }
  }

  handleClick (item) {
    this.setState({ lists: item })
  }

  handlePreview (itemPreview) {
    console.log('handlePreview', itemPreview)
    this.setState({ preview: itemPreview })
  }

  render() {
    const { lists } = this.state.lists
    const arrayOrders = this.state.lists.length
    console.log('arrayOrders', arrayOrders)
    console.log('this.state.lists')
    return (
      <div>
      {this.props.id.comandas.id === 1
        ?  <div className='content-comandas'>
            <div className='content-list-users'>
              <h2>Usuarios</h2>
              {this.props.comandas.map((item) => {
                return(
                  <div class='list'>
                    <p
                      style={{ cursor: 'pointer' }}
                      onClick={() => this.handleClick(item.orders)}>
                      {item.name}
                    </p>
                  </div>
                )
              })}
            </div>
            <div className='content-list-comandas'>
            <h2>Comandas</h2>
              { arrayOrders === 0
                ? 'aún no tienes comandas'
                : <p
                  onClick={() => this.handlePreview(this.state.lists[0]['comandas'])}
                  style={{ cursor: 'pointer' }} >Comandas {arrayOrders}</p>
              }
            </div>
            <div className='content-preview-comandas'>
            <h2>Detalles</h2>
              <div>
              {this.state.preview.map((item) => {
                return (
                  <p>{item.name}</p>
                )
              })}</div>
            </div>
          </div>
        : 'Espere ...'}
      </div>
    )
  }
}

export default ComandasView
