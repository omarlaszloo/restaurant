import { connect } from 'react-redux'
import {
  fetchMenu,
  handleAdd,
  handleDeleteItem,
  updateUserComanda
} from '../HomeActions'
import { fetchComandas } from '../../comandas/ComandasActions'

import HomeView from '../components/HomeView'

import { getMenu } from '../HomeReducer'
import { getComandas } from '../../comandas/ComandasReducer'

const mapDispatchToProps = {
  fetchMenu,
  fetchComandas,
  handleAdd,
  handleDeleteItem,
  updateUserComanda
}

const mapStateToProps = state => {
  return {
    menu: getMenu(state),
    comandas: getComandas(state),
    id: state  
  }
}

const temp = connect(mapStateToProps, mapDispatchToProps)(HomeView)

class Container extends temp {
  componentWillMount () {
    this.store.dispatch(fetchMenu())
    this.store.dispatch(fetchComandas())
  }

}

export default Container
