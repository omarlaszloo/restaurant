import React, { Component, PropTypes } from 'react'
import GridMenu from '../../../components/GridMenu'
import ActionsBtn from '../../../components/ActionsBtn'
import HomeForm from './HomeForm'


import './HomeView.css'

class HomeView extends Component {

  submitForm = () => {
    this.refs.form.getWrappedInstance().submit()
  }

  render() {
    const { menu } = this.props
    const { list } = this.props.id.home
    return(
      <div>
        {this.props.id.home.id === 1
          ? <div className='main-grid'>
              <GridMenu
                handleAdd={this.props.handleAdd}
                menu={menu}
                gridClass='grid-menu'
                cardFood='card-food'
                cardDescription='card-description'
              />
              <div className='grid-orders'>
                <div className='content'>
                  <div className='table'>
                    <HomeForm
                      ref='form'
                      onSubmit={this.props.updateUserComanda}
                      actions={this.props.updateUserComanda}
                      users={this.props.id}
                      list={list}
                      btnDelete='btn-delete-item'
                      handleDeleteItem={this.props.handleDeleteItem}/>
                  </div>
                </div>
              </div>
            </div>
          : 'espere...'}
        </div>
    )
  }
}

HomeView.propTypes = {
  menu: PropTypes.array.isRequired,
  handleAdd: PropTypes.func,
  handleDeleteItem: PropTypes.func,
  updateUserComanda: PropTypes.func
}

export default HomeView
