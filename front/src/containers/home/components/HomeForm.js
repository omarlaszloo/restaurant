import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import NumberFormat from 'react-number-format'
import ActionsBtn from '../../../components/ActionsBtn'


const HomeForm = props => {

  return(
    <div>
    {props.users.comandas.id === 1
      ? <form onSubmit={props.handleSubmit} className='form-products'>
        <p>Nota: Es importante que selecciones tu usuario para poder crear una comanda </p>
        <div className='description'>
          <label>Usuarios</label>
          <div>
            <Field name='id' component='select'>
              {props.users.comandas.data.map((item) => {
                return(
                  <option value={item._id}>{item.name}</option>
                )
              })}
            </Field>
          </div>
        </div>
        <table style={{ width:'100%', textAlign: 'center' }}>
          <tr>
            <th>Comida</th>
            <th>Descripción</th>
            <th>Costo</th>
            <th>Borrar</th>
          </tr>
          {props.list.map((itemComds) => {
            return (
              <tr>
                <td style={{textTransform: 'uppercase' }}>{itemComds.name}</td>
                <td style={{textTransform: 'uppercase' }}>{itemComds.description}</td>
                <td>
                  <NumberFormat
                  value={itemComds.price}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'$'} />
                </td>
                <td>
                  <button
                  onClick={() => props.handleDeleteItem(itemComds)}
                  className={props.btnDelete}>
                  X
                  </button>
                </td>
              </tr>
            )
          })
        }
        </table>
        <div className='' style={{width: '50%' }}>
          <ActionsBtn
            titleCancel='Cancelar'
            titleAcept='Generar comanda'
            handleClick={props.actions}
          />
        </div>
      </form>
      : 'cargando usuarios...'}
    </div>
  )
}

const component = reduxForm({ form: 'example' })(HomeForm)

export default connect(null, null, null, { withRef: true })(component)
