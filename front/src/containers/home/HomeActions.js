import { callApi, callApiByForm } from '../../util/apiCaller'


// ------------------------------------
// Constants
// ------------------------------------
export const ADD_MENU = 'ADD_MENU'
export const ADD = 'ADD'
export const DELETE_MENU = 'DELETE_MENU'

// ------------------------------------
// Actions
// ------------------------------------
export const addMenus = menu => ({ type: ADD_MENU, menu })

export const addMenu = id => ({ type: ADD, id })

export const deleteMenu = id => ({ type: DELETE_MENU, id }) 

export const fetchMenu = () => {
  return dispatch => (callApi('menu').then(menu => dispatch(addMenus(menu.menu), console.log('menus*****', menu))))
}

export const addComandaRequest = data => {
  console.log('addComandaRequest', data)
  return dispatch => (callApiByForm(`user/${data.id}`, 'PUT', data).then(res => {
      console.log('res >>>', res)
  }))
}

export const handleAdd = id => {
  return dispatch => {
    dispatch(addMenu(id))
  }
}

export const handleDeleteItem = id => {
  return dispatch => {
    dispatch(deleteMenu(id._id))
  }
}

export const updateUserComanda = dataForm => {
  console.log('updateUserComanda', dataForm)



  return(dispatch, getState) => {
    // const listComandas =
    // console.log('getState', getState().home.list)
    dataForm.comanda = getState().home.list
    /* const upload = [
        {name: 'burrito1', description: 'hahsd', price: 40},
        {name: 'burrito1', description: 'hahsd', price: 40},
        {name: 'burrito1', description: 'hahsd', price: 40},
        {name: 'burrito1', description: 'hahsd', price: 40},
        {name: 'burrito1', description: 'hahsd', price: 40}
    ] */
    // console.log('>>>>>', dataForm)
    dataForm.comanda = { comandas: getState().home.list }
    console.log('dataForm >>>>', dataForm)
    dispatch(addComandaRequest(dataForm))
    //if (dataForm.orders){

    //}
  }
}