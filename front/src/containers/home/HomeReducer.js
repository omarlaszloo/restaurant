import _ from 'lodash'
import {
  ADD_MENU,
  ADD,
  DELETE_MENU,
 } from './HomeActions'


// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [ADD_MENU]: (state, action) => ({
    data: action.menu,
    id: 1,
    list: []
  }),
  [ADD]: (state, action) => ({
    data: state.data,
    id: 1,
    list: [...state.list, action.id]
  }),
  [DELETE_MENU]: (state, action) => ({
    data: state.data,
    id: 1,
    list: state.list.filter(item => item._id !== action.id)
  })
}

// Initial State
const initialState = {
  data: [],
  id: 0,
  list: []
}

export default function HomeReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

// get all menu
export const getMenu = state => state.home.data