import { injectReducer } from '../../store/reducers'
import HomeContainer from './containers/HomeContainer'
import reducerComandas from '../comandas/ComandasReducer'
import reducer from './HomeReducer'


export default(store) => ({
  path: 'home',
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      /*  Add the reducer to the store on key 'counter'  */
      injectReducer(store, {
        key: 'home',
        reducer
      })

      injectReducer(store, {
        key: 'comandas',
        reducer: reducerComandas
      })
    
      /*  Return getComponent   */
      cb(null, HomeContainer)

      /* Webpack named bundle   */
    }, 'HomeView')
  }
})
