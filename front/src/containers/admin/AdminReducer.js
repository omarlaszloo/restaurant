import _ from 'lodash'
import {
  ADD,
  DELETE,
} from './AdminActions'


// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [ADD]: (state, action) => ({
    data: [...state.data, action.data],
    id: 1,
  }),
  [DELETE]: (state, action) => ({
    data: state.list.filter(item => item._id !== action.id),
    id: 1
  })
}

// Initial State
const initialState = {
  data: [],
  id: 0
}

export default function AdminReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
