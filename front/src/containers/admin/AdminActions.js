import { callApi, callApiByForm } from '../../util/apiCaller'

export const LIST_MENU = 'LIST_MENU'
export const ADD = 'ADD'
export const DELETE = 'DELETE'

export const add = data => ({ type: ADD, data })

export const addRequest = data => {
    return dispatch => (callApiByForm('menu', 'post', data).then(res => {
        console.log('res >>>', res)
        dispatch(add(data))
    }))
}


export const submitAdminForm = dataForm => {
    return dispatch => {
        dispatch(addRequest(dataForm))
    }
}