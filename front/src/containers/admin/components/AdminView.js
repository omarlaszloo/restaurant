import React, { Component, PropTypes } from 'react'
import AdminViewForm from './AdminViewForm'
import './AdminView.css'

class AdminView extends Component {

  submitForm = () => {
    this.refs.form.getWrappedInstance().submit()
  }

  render () {
    console.log('submitAdminForm', this.props)
    return (
      <div className='content-form'>
        <AdminViewForm
          ref='form'
          submitForm={this.props.submitForm}
          onSubmit={this.props.submitAdminForm}
          actions={this.props.submitAdminForm}
        />
      </div>    
    )
  }
}

AdminView.propTypes = {
  submitAdminForm: PropTypes.func
}

export default AdminView


