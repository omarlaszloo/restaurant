import React, { Component, PropTypes } from 'react'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import ActionsBtn from '../../../components/ActionsBtn'

class AdminViewForm extends Component {
  render () {
    console.log('todos los props', this.props)
    return (
      <form onSubmit={this.props.handleSubmit} className='form-products'>
          <h2>* Agregar productos</h2>
          <div>
            <label htmlFor='Producto'>Producto</label>
            <div>
              <Field
                name='name'
                component='input'
                type='text'
                placeholder='producto'
              />
            </div>
          </div>
          <div>
            <label htmlFor='description'>description</label>
            <div>
              <Field
                name='description'
                component='input'
                type='text'
                placeholder='description'
              />
            </div>
          </div>
          <div>
            <label htmlFor='precio'>Precio</label>
            <div>
              <Field
                name='price'
                component='input'
                type='text'
                placeholder='precio'
              />
            </div>
          </div>
          <ActionsBtn
            titleCancel='Cancelar'
            titleAcept='Gurdar'
            handleClick={this.props.actions}
          />
        </form>
    )
  }
}

AdminViewForm.propTypes = {
  actions: PropTypes.func
}

const component = reduxForm({ form: 'example' })(AdminViewForm)

export default connect(null, null, null, { withRef: true })(component)

