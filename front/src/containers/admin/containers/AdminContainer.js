import { connect } from 'react-redux'
import {submitAdminForm} from '../AdminActions'

import AdminView from '../components/AdminView'

const mapDispatchToProps = {
  submitAdminForm,
}

/* const mapStateToProps = (state, dispatch) => {
  return {
    menu: getMenu(state),
    id: state  
  }
} */

const temp = connect(null, mapDispatchToProps)(AdminView)

class Container extends temp {
  componentWillMount () {
    // 
  }

}

export default Container
