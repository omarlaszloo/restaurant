import { injectReducer } from '../../store/reducers'
import HomeContainer from './containers/AdminContainer'
import reducer from './AdminReducer'


export default(store) => ({
  path: 'admin',
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      /*  Add the reducer to the store on key 'counter'  */
      injectReducer(store, {
        key: 'admin',
        reducer
      })
    
      /*  Return getComponent   */
      cb(null, HomeContainer)

      /* Webpack named bundle   */
    }, 'AdminView')
  }
})
