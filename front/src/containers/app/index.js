import React from 'react'
import { Provider } from 'react-redux'
import { Router, browserHistory } from 'react-router'

const App = props => (
  <main>
    <Provider store={props.store}>
      <Router history={browserHistory} children={props.routes} />
    </Provider>
  </main>
)

export default App
