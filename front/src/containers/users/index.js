import { injectReducer } from '../../store/reducers'
import UsersContainer from './containers/UsersContainer'
import reducer from './UsersReducer'


export default(store) => ({
  path: 'users',
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      /*  Add the reducer to the store on key 'counter'  */
      injectReducer(store, {
        key: 'users',
        reducer
      })
    
      /*  Return getComponent   */
      cb(null, UsersContainer)

      /* Webpack named bundle   */
    }, 'UsersView')
  }
})
