import { callApiByForm } from '../../util/apiCaller'

export const ADD_USER = 'ADD_USER'
export const DELETE = 'DELETE'

export const addUser = user => ({ type: ADD_USER, user })

export const addUserRequest = data => {
    return dispatch => (callApiByForm('user', 'POST', data).then(res => {
        console.log('res >>>', res)
        dispatch(addUser(data))
    }))
}


export const submitUserForm = dataForm => {
    return dispatch => {
        dispatch(addUserRequest(dataForm))
    }
}