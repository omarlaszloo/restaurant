import React, { Component, PropTypes } from 'react'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import ActionsBtn from '../../../components/ActionsBtn'

class UsersForm extends Component {
  render () {
    console.log('todos los props', this.props)
    return (
      <form onSubmit={this.props.handleSubmit} className='form-users'>
        <h2>* Agregar Usuarios</h2>
        <div>
          <label htmlFor='Nombre'>Nombre</label>
          <div>
            <Field
              name='name'
              component='input'
              type='text'
              placeholder='nombre'
            />
          </div>
        </div>
        <div>
          <label htmlFor='apellidos'>Apellido(s)</label>
          <div>
            <Field
              name='last_name'
              component='input'
              type='text'
              placeholder='apellido(s)'
            />
          </div>
        </div>
        <div>
          <label htmlFor='sexo'>Sexo</label>
          <div>
            <Field
              name='sexo'
              component='input'
              type='text'
              placeholder='sexo'
            />
          </div>
        </div>
        <div>
          <label htmlFor='edad'>edad</label>
          <div>
            <Field
              name='age'
              component='input'
              type='text'
              placeholder='edad'
            />
          </div>
        </div>
        <ActionsBtn
          titleCancel='Cancelar'
          titleAcept='Guardar'
          handleClick={this.props.actions}
        />
      </form>
    )
  }
}

UsersForm.propTypes = {
  actions: PropTypes.func
}

const component = reduxForm({ form: 'example' })(UsersForm)

export default connect(null, null, null, { withRef: true })(component)

