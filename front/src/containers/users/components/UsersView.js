import React, { Component, PropTypes } from 'react'
import UsersForm from './UsersForm'
import './Users.css'

class UsersView extends Component {

  submitForm = () => {
    this.refs.form.getWrappedInstance().submit()
  }

  render () {
    console.log('submitUserForm', this.props)
    return (
      <div className='content-users'>
        <UsersForm
          ref='form'
          submitForm={this.props.submitForm}
          onSubmit={this.props.submitUserForm}
          actions={this.props.submitUserForm}
        />
      </div>    
    )
  }
}

UsersView.propTypes = {
  submitUserForm: PropTypes.func
}

export default UsersView


