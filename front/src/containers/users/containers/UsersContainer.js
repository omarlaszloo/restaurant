import { connect } from 'react-redux'
import { submitUserForm } from '../UsersActions'

import UsersView from '../components/UsersView'

const mapDispatchToProps = {
  submitUserForm,
}

/* const mapStateToProps = (state, dispatch) => {
  return {
    menu: getMenu(state),
    id: state  
  }
} */

const temp = connect(null, mapDispatchToProps)(UsersView)

class Container extends temp {
  componentWillMount () {
    // 
  }

}

export default Container
