'use strict'

import Users from './Users.model'

export const getAllUsers = (req, res) => {
  Users.find({})
  .exec((err, users) => { return res.json({users}) })
}


export const addUsers = (req, res) => {
  let body = req.body
  console.log('body', body)
  let user = new Users({
    name: body.name,
    last_name: body.last_name,
    sexo: body.sexo,
    age: body.age,
    orders: body.orders
  })

  user.save((err, usersSave) => {
    if (err) {
      res.status(400).json({
        status: false,
        message: 'error al crear el usuario'
      })
    }
    res.status(200).json({
      status: 200,
      message: 'success',
      usersSave: usersSave
    })
  })
}

export const deleteUser = (req, res) => {
  let id = req.params.id

} 

export const updateUsers = (req, res) => {
  console.log('updateUsers')
  let id = req.params.id
  let body = req.body
  console.log('body', body)

  Users.findById(id, (err, users) => {
    if (err) {
      res.status(500).json({
        status: 500,
        err
      })
    }

    if (!users) {
      res.status(400).json({
        status: 400,
        message: 'El usuario con el id' + id + 'no existe'
      })
    }

    users.orders = body.comanda
    /* users.last_name = body.last_name
    users.sexo = body.sexo
    users.age = body.age */

    users.save((err, usersUpdate) =>{
      if (err) {
        res.status(500).json({
          status: 500,
          message: 'Error al actualizar el usuario'
        })
      }
      res.status(200).json({
        status: 200,
        users: usersUpdate
      })
    })

  })
}
