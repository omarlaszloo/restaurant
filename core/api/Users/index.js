'use strict'

import { Router } from 'express'
import * as AllUsers from './Users.controller'
const router = new Router()


// get all menu
router.get('/', AllUsers.getAllUsers)
// add menu
router.post('/', AllUsers.addUsers)
// update user
router.put('/:id', AllUsers.updateUsers)
// delete by type
router.delete('/:id', AllUsers.deleteUser)

export default router
