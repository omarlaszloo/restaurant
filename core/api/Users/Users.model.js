'use strict'

import mongoose from 'mongoose'

let Schema = mongoose.Schema

let Users = new Schema({
  name: {
    type: String,
    required: [true, 'Es necesario agregar un nombre']
  },
  last_name: {
    type: String,
    required: [true, 'Es necesario agregar una description']
  },
  sexo: {
    type: String,
    required: [true, 'Es necesario agregar una description']
  },
  age: {
    type: String,
    required: [true, 'Es necesario agregar una description']
  },
  orders: [
    { comandas:[{ name: String, description: String, price: String }]}]
})


module.exports = mongoose.model('Users', Users)
