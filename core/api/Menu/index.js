'use strict'

import { Router } from 'express'
import * as AllMenu from './Menu.controller'
const router = new Router()


// get all menu
router.get('/', AllMenu.getAllMenu)
// add menu
router.post('/', AllMenu.addMenu)
// delete by type
router.delete('/:id', AllMenu.deleteMenu)

export default router
