'use strict'

import mongoose from 'mongoose'

let Schema = mongoose.Schema

let MenuByDay = new Schema({
  name: {
    type: String,
    required: [true, 'Es necesario agregar un nombre']
  },
  description: {
    type: String,
    required: [true, 'Es necesario agregar una description']
  },
  price: {
    type: Number,
    required: [true, 'Es necesario agregar una description']
  }
})


module.exports = mongoose.model('Menu', MenuByDay)
