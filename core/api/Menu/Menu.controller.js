'use strict'

import Menu from './Menu.model'

export const getAllMenu = (req, res) => {
  Menu.find({})
  .exec((err, menu) => { return res.json({menu}) })
}


export const addMenu = (req, res) => {
  let body = req.body
  console.log('body', body)
  let menu = new Menu({
    name: body.name,
    description: body.description,
    price: body.price
  })

  menu.save((err, menuSave) => {
    if (err) {
      res.status(400).json({
        status: false,
        message: 'error al crear el producto'
      })
    }
    res.status(200).json({
      status: 200,
      message: 'success',
      menu: menuSave
    })
  })
}

export const deleteMenu = (req, res) => {
  let body = req.body
  console.log('body', body)
}
