'use strict'

import { Router } from 'express'
import * as AllCommandas from './Commandas.controller'
const router = new Router()


// get all menu
router.get('/', AllCommandas.getAllCommandas)
// add menu
router.post('/', AllCommandas.addCommanda)
// delete by type
router.delete('/:id', AllCommandas.deleteCommanda)

export default router
