'use strict'

import mongoose from 'mongoose'

const Mongodb = () => {
  mongoose.connection.openUri('mongodb://localhost:27017/restaurant', (err, res) => {
    if (err) throw error
    console.log('mongodb \x1b[32m%s\x1b[0m', 'restaurant >>> online')
  })
}

export default Mongodb
