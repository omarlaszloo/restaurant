'use strict'

import express from 'express'
import bodyParser from 'body-parser'
import Mongodb from './connect/mongo.db'
import Menu from './api/Menu'
import Users from './api/Users'

let app = express()


app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
  res.header('Access-Control-Allow-Methods', 'GET,PUT,PATCH,POST,DELETE')
  if (req.method === 'OPTIONS') return res.send(200)
  next()
})

// here connect mongo
Mongodb()

//router
app.use('/api/menu', Menu)
app.use('/api/user', Users)


app.listen(4000, () => {
  console.log('Listen port 4000: \x1b[32m%s\x1b[0m', 'Online')
})
